# Revisão Rafael

### Pontos positivos

- Solucionou o problema
- Configuração de tooling
- Normalizou a requisição para componentes de exibição
- Loading de requisição
- Utilizou API de geolocalização

### O que eu sugeriria como melhorias

- [ ] **@testing-library/react** em dependências no `package.json`
- [ ] **tools** para padronização no projeto, eg `eslint` `prettier` `editorconfig`
- [ ] Implementar testes ao menos de unidade
- [ ] Qual a diferença entre as funções `formatData` de `utils.js` e `Main.js`?
- [ ] Porque utilizar estilos normais se está utilizando `styled-components`?
- [ ] Definir paleta para manutenção mais simples e trazer mais consistência visual.
- [ ] Há abstrações que poderiam ser feitas para componentes, eg `Table.js L50`
- [ ] Poderia ter criado camada de serviço para encapsular a requisição e mover métodos de normalização para lá.
- [ ] Quando a requisição falha, o loading fica pra sempre? O usuário fica esperando até quando?
- [ ] Maior legibilidade na definição de nomes, o que é `monthFo` ou `og`?
- [ ] Utilizar `PropTypes`

### Dúvidas

- [ ] Conhece a API de hooks?
- [ ] O que conhece sobre testes?
- [ ] Qual a sua familiariedade com abstração?
- [ ] Conhece alguma coisa de animações em React?
