# Revisão Gabriel Zanluca

### Pontos positivos

- Solucionou o problema
- Começou a escrever alguns cenários de testes
- Tratou os casos que não são de sucesso
- Componentes bem idealizados

### O que eu sugeriria como melhorias

- [ ] Criou uma camada de serviços, mas poderia ter normalizado os dados na mesma.
- [ ] Não versionar o API KEY
- [ ] **tools** para padronização no projeto, eg `eslint` `prettier` `editorconfig`
- [ ] Definir paleta para manutenção mais simples e trazer mais consistência visual
- [ ] Utilizar `PropTypes`
- [ ] Condicional poderia ser simplificado pra maior legibilidade `HeaderDays.js` em `L7`

### Dúvidas

- [ ] Conhece a API de hooks?
- [ ] Já teve outras experiências com testes? Unitário, integração ou outros
- [ ] Qual o seu contato com open source e comunidades?
- [ ] O que é a abstração para você?
- [ ] Qual a sua familiariedade com UI/UX?
- [ ] O que conhece sobre visualização de dados? (tabelas, mapas, gráficos)
