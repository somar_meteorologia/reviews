# Revisão Thiago Novato

### Pontos positivos

- [ ] Pattern estrutural **Ducks**
- [ ] Estilos separados
- [ ] Solucionou o problema
- [ ] Serviu a aplicação em um host

### O que eu sugeriria como melhorias

- [ ] Guardou o token no `.env`, porém versionou 😟
- [ ] **react-scripts** em dependências no `package.json`
- [ ] **tools** para padronização no projeto, eg `eslint` `prettier` `editorconfig`.
- [ ] Paleta de cores com diversos tons semelhantes, isso poderia ser enxugado trazendo mais consistência visual.
- [ ] Código em inglês e português, criar consistência.
- [ ] Criar camada de serviço ou anticorrupção para normalizar o request e não gerar acessos assim `data.points.forecast.temperature_daily_max`
- [ ] Implementar testes ao menos de unidade
- [ ] Utilizar `PropTypes`
- [ ] Complexidade ciclomática desnecessária em alguns lugares
- [ ] `Bars` e `Grafico` poderia ser um stateless component, porém tiveram a responsabilidade de normalizar os dados.

### Dúvidas

- [ ] Porque utilizou o `Axios`?
- [ ] Conhece a API de hooks?
- [ ] Porque utilizou Redux?
- [ ] O que conhece sobre testes?

### Complexidade

- `Grafico/index.js` - 46 ~ 59
- `Barras/index.js` - 20 ~ 36
