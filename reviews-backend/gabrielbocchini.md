# Gabriel Bocchini

### Pontos positivos

- Utilizou:
  - list compreension
  - desconstrução de dict
  - webargs

- integração com o travis
- deploy no heroku
- swagger integrado

### Pontos negativos
- Não resolveu o problema de padronização da resposta
- Integrou o pandas para ler um csv
- A verificação do nome da cidade ocorre diversas vezes sem necessidade.

### Melhorias sugeridas
 - utilizar bibliotecas como flake8 e black.
 - Variaveis como temp1 e temp2 não representam o que possuem.
 - observed_data possui uma lista, mas seu nome não lembra isso, e o seu conteudo faz referência somente as cidades e suas latitudes.
 - Metódos para requisição de dados, get para listar as cidades e post para receber os dados de previsão da cidade.
 - diminuir a quantidade de repetição de código.
 - teste unitário.

### Dúvidas
 - Porque webargs frente ao marshmallow?
 - Doc API vs Swagger puro, porque?
 - Quanto conhece de classe e herança?
 - Conhece testes unitários?
 - Quanto conhece de gunicorn?
 -
